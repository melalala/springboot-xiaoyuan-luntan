package com.forum.dao;

import com.forum.entity.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TagDao {
    /**
     * 执行新增模块
     * @param tag
     * @return
     */
    int saveTag(Tag tag);

    Tag getTag(Long id);

    Tag getTagByName(String name);
    Tag getTagById(int id);

    List<Tag> getAllTag();
    /**
     * 查询帖子所有分类
     * @return
     */
    List<Tag> getForumTag();

    int updateTag(Tag tag);

    int deleteTag(Long id);

    int countTag();
}
