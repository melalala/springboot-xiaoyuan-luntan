package com.forum.dao;

import com.forum.entity.Forum;
import com.forum.entity.ForumAndTag;
import com.forum.entity.RankForum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ForumDao {

    Forum getForum(Long id);  //后台展示帖子
    void updateView(Long id);
    Forum getDetailedForum(@Param("id") Long id);  //帖子详情

    //获取所有帖子
    List<Forum> getAllForum();

    List<Forum> getByTypeId(Long typeId);  //根据类型id获取帖子

    List<Forum> getByTagId(Long tagId);  //根据标签id获取帖子

    List<Forum> getIndexForum();  //主页帖子展示

    List<Forum> getAllRecommendForum();  //推荐帖子展示

    List<Forum> getSearchForum(String query);  //全局搜索帖子

    List<Forum> searchAllForum(Forum forum);  //后台根据标题、分类、推荐搜索帖子

    List<String> findGroupYear();  //查询所有年份，返回一个集合

    List<Forum> getForumTadIds();
    List<Forum> findForumAndTagsByYear(@Param("year") String year);
    int saveForum(Forum forum);

    int saveForumAndTag(ForumAndTag forumAndTag);

    int updateForum(Forum forum);

    int deleteForum(Long id);
    List<RankForum> findForumByRank();
    List<RankForum> getNewForum();
//    点赞相关
    Long getLike(Long id);
//    void addLike(Long forumId);
//    int getLikeByUser(Long userId,Long forumId);
//    void addForum_Like(Long forumId,Long userId);
//    void removeForum_Like(Long forumId,Long userId);
//    void cancelLike(Long forumId);


    int countView();


    void addLike(Long id);
}
