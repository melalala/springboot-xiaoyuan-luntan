package com.forum.dao;

import com.forum.entity.Reply;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 田勇
 * @date 2021/4/1 22:08
 */
@Repository
@Mapper
public interface ReplyDao {
    List<Reply> findAll();

    void del(int id);
}
