package com.forum.dao;

import com.forum.entity.Link;
import org.apache.ibatis.annotations.Mapper;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 田勇
 * @date 2021/3/30 15:51
 */
@Mapper
@Repository
public interface LinkDao {
    public int deleteLink(Integer id);

    int saveLink(Link link);

    List<Link> getAllLink();

    int countLink();
}
