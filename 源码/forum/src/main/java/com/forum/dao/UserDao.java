package com.forum.dao;


import com.forum.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserDao {
//    @Select("select * from t_user where username=#{username} and password=#{password}")
    User queryByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    void addUser(User user);
    List<User> getAllUser();

    int deleteUser(int id);

    int count();

    void update(String password, String username);

//    User queryByUsernameAndPassword2(@Param("username") String username, @Param("password") String password);
}
