package com.forum.dao;


import com.forum.entity.Message;
import com.forum.entity.Reply;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MessageDao {
    List<Message> findAllMessage();
    List<Message> getIndexMessage();
    void saveMessage(Message message);

    void saveReplyMessage(Reply reply);

    List<Message> findAll();

    void deleteById(int id);

}
