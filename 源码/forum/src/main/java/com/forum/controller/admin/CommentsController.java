package com.forum.controller.admin;

import com.forum.entity.Comment;
import com.forum.service.CommentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 田勇
 * @date 2021/4/1 23:39
 */
@Controller
@RequestMapping("/admin")
public class CommentsController {
    @Autowired
    CommentService commentService;
    @GetMapping("/comment")
    public String findAll(@RequestParam(required = false,defaultValue = "1",value = "pagenum")int pagenum, Model model){
        List<Comment> allComment = commentService.getAllComment();
        PageHelper.startPage(pagenum,5);
        PageInfo<Comment> commentPageInfo = new PageInfo<>(allComment);
        model.addAttribute("pageInfo",commentPageInfo);
        return "admin/comment";

    }
    @GetMapping("/comment/{id}/delete")
    public String del(@PathVariable Integer id){
        commentService.delById(id);
        return "redirect:/admin/comment";
    }
}
