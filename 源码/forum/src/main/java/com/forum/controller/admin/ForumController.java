package com.forum.controller.admin;


import com.forum.entity.Forum;
import com.forum.entity.User;
import com.forum.service.ForumService;
import com.forum.service.TagService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class ForumController {

    @Autowired
    private ForumService forumService;

    @Autowired
    private TagService tagService;

    public void setTypeAndTag(Model model) {

        model.addAttribute("tags", tagService.getAllTag());
    }

    @GetMapping("/forums")  //后台显示帖子列表
    public String forums(@RequestParam(required = false,defaultValue = "1",value = "pagenum")int pagenum, Model model){
        PageHelper.startPage(pagenum, 5);
        List<Forum> allForum = forumService.getAllForum();
        //得到分页结果对象
        PageInfo pageInfo = new PageInfo(allForum);
        model.addAttribute("pageInfo", pageInfo);
        setTypeAndTag(model);  //查询类型和标签
        return "admin/forums";
    }


    @GetMapping("/forums/input") //去新增帖子页面
    public String toAddForum(Model model){
        model.addAttribute("forum", new Forum());  //返回一个forum对象给前端th:object
        setTypeAndTag(model);
        return "admin/forums-input";
    }
    @PostMapping("/forums") //新增
    public String addForum(Forum forum, HttpSession session, RedirectAttributes attributes){
        //设置user属性
        forum.setUser((User) session.getAttribute("admin"));
        //设置用户id
        forum.setUserId(forum.getUser().getId());
//        //设置forum的type
        //给forum中的List<Tag>赋值
        forum.setTags(tagService.getTagByString(forum.getTagIds()));
        forumService.saveForum(forum);
        attributes.addFlashAttribute("msg", "新增成功");
        return "redirect:/admin/forums";
    }

    @GetMapping("/forums/{id}/edit") //去编辑帖子页面
    public String toEditForum(@PathVariable Long id, Model model){
        Forum forum = forumService.getForum(id);
        forum.init();   //将tags集合转换为tagIds字符串
        model.addAttribute("forum", forum);     //返回一个forum对象给前端th:object
        setTypeAndTag(model);
        return "admin/forums-edit";
    }
    @PostMapping("/forums/edit") //编辑
    public String editForum(Forum forum, HttpSession session, RedirectAttributes attributes){
        //设置user属性
        forum.setUser((User) session.getAttribute("admin"));
        //设置用户id
        forum.setUserId(forum.getUser().getId());
//        //设置forum的type
        //给forum中的List<Tag>赋值
        forum.setTags(tagService.getTagByString(forum.getTagIds()));
        System.out.println(forum);
        forumService.updateForum(forum);
        attributes.addFlashAttribute("msg", "编辑成功");
        return "redirect:/admin/forums";
    }


    @GetMapping("/forums/{id}/delete")
    public String deleteForums(@PathVariable Long id, RedirectAttributes attributes){
        forumService.deleteForum(id);
        attributes.addFlashAttribute("msg", "删除成功");
        return "redirect:/admin/forums";
    }

    @PostMapping("/forums/search")  //按条件查询帖子
    public String searchForums(Forum forum, @RequestParam(required = false,defaultValue = "1",value = "pagenum")int pagenum, Model model){
        PageHelper.startPage(pagenum, 5);
        List<Forum> allForum = forumService.searchAllForum(forum);
        //得到分页结果对象
        PageInfo pageInfo = new PageInfo(allForum);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("message", "查询成功");
        setTypeAndTag(model);
        return "forums";
    }

}
