package com.forum.controller.admin;

import com.forum.entity.Link;
import com.forum.entity.Message;
import com.forum.entity.Notice;
import com.forum.entity.Reply;
import com.forum.service.MessageService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author 田勇
 * @date 2021/4/1 15:25
 */
@Controller
@RequestMapping("/admin")
public class MessagesController {

    @Autowired
    private MessageService messageService;
    @GetMapping("/message")
    public String find (@RequestParam(required = false,defaultValue = "1",value = "pagenum")int pagenum,Model model) {
        PageHelper.startPage(pagenum, 5);
        List<Message> messages=messageService.findAll();

        PageInfo<Message> pageInfo = new PageInfo<>(messages);
        model.addAttribute("pageInfo",pageInfo);


        return "admin/message";
    }
    /**
     * 执行删除
     * @returnid
     */
    @GetMapping("/message/{id}/delete")
    public String delete(@PathVariable int id){
        messageService.deleteById(id);
        return "redirect:/admin/message";
    }

}
