package com.forum.controller;

import com.forum.entity.Forum;
import com.forum.entity.RankForum;
import com.forum.entity.Tag;
import com.forum.service.ForumService;
import com.forum.service.TagService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class TagShowController {

    @Autowired
    private TagService tagService;

    @Autowired
    private ForumService forumService;

    @GetMapping("/tags/{id}")
    public String findTypes(@PathVariable Long id, @RequestParam(required = false,defaultValue = "1",value = "pagenum")int pagenum,
                        Model model){
        PageHelper.startPage(pagenum, 100);  //开启分页
        List<Tag> tags = tagService.getForumTag();
        //-1从导航点过来的
        if (id == -1){
//            如果id=-1表示这是从导航栏进的，从数据库查找排在第一位的标签，get(0)精妙
            id = tags.get(0).getId();
        }
        List<Forum> forums = forumService.getByTagId(id);
        for (Forum forum : forums) {
            System.out.println(forum);
        }
        PageInfo<Forum> pageInfo = new PageInfo<>(forums);
        List<RankForum> lankForums=forumService.findForumByLank();

        model.addAttribute("lankForums", lankForums);
        model.addAttribute("tags", tags);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("activeTagId", id);
        return "tags";
    }
}
