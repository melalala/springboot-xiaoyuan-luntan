package com.forum.controller;

import com.forum.entity.*;
import com.forum.service.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private ForumService forumService;

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private TagService tagService;
    @Autowired
    private MessageService messageService;

    @Autowired
    private LinkService linkService;

    private HttpSession session;



    @GetMapping("/")
    public String toIndex(@RequestParam(required = false,defaultValue = "1",value = "pageNum")int pagenum, Model model){

        PageHelper.startPage(pagenum,5);
//
        List<Forum> allForum = forumService.getIndexForum();
//
        List<Tag> allTag = tagService.getForumTag();  //获取帖子的标签(联表查询)

        //得到分页结果对象
        PageInfo pageInfo = new PageInfo(allForum);
        System.out.println(pageInfo);

        List<RankForum> lankForums=forumService.findForumByLank();
        List<RankForum> newForum = forumService.getNewForum();

        model.addAttribute("lankForums", lankForums);
        model.addAttribute("newForum",newForum);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("tags", allTag);
        model.addAttribute("notices",noticeService.findAllNotice());
        model.addAttribute("messages",messageService.getIndexMessage());

        List<Link> allLink =linkService.getAllLink();
        //得到分页结果对象

        model.addAttribute("f", allLink);


        return "index";
    }


    @PostMapping("/search")
    public String toSearch(@RequestParam(required = false,defaultValue = "1",value = "pagenum")int pagenum,
                         @RequestParam String query, Model model){

        PageHelper.startPage(pagenum, 5);
        List<Forum> searchForum = forumService.getSearchForum(query);
        PageInfo pageInfo = new PageInfo(searchForum);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("query", query);
        return "search";
    }

    @GetMapping("/forum/{id}")
    public String toDetail(@PathVariable Long id, Model model, HttpSession session){
        Forum forum = forumService.getDetailedForum(id);
        forumService.updateView(id);


        model.addAttribute("forum", forum);
        return "forum";
    }
    @GetMapping("/forum/like/{id}")
    public String getLike(@PathVariable Long id, Forum forum,HttpSession session){
        forumService.addLike(id);
        User user =(User)session.getAttribute("user");
        session.setAttribute("user",user);
        return "redirect:/forum/"+id;
    }

    public void findAllTag(Model model) {

        model.addAttribute("tags", tagService.getAllTag());
    }
    @GetMapping("/publish")
    public String toPublic(Model model,HttpSession session,RedirectAttributes attributes){
        model.addAttribute("forum", new Forum());  //返回一个forum对象给前端th:object
        findAllTag(model);
        attributes.addFlashAttribute("msg1", "请先登录在发布哦");
        User user = (User)session.getAttribute("user");
        if (user==null){
            return "redirect:/user";
        }else {
            return "publish";

        }

    }


    @PostMapping("/add") //新增
    public String addForum(@RequestParam(required = false,defaultValue = "1",value = "pageNum")int pagenum,Model model,Forum forum, HttpSession session, RedirectAttributes attributes){
        //设置user属性
        forum.setUser((User) session.getAttribute("user"));
        //设置用户id
        forum.setUserId(forum.getUser().getId());
//        //设置forum的type
        //给forum中的List<Tag>赋值
        forum.setTags(tagService.getTagByString(forum.getTagIds()));
        forumService.saveForum(forum);
       attributes.addFlashAttribute("msg", "发布成功");
        return "redirect:/";
    }

}
