package com.forum.controller.admin;

import com.forum.entity.Tag;
import com.forum.entity.User;
import com.forum.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * @author 田勇
 * @date 2021/3/30 14:31
 */
@Controller
@RequestMapping("/admin")
public class UserController {

    @Autowired
    private UserService userService;
    @GetMapping("/userAll")
    public String listUser(@RequestParam(required = false,defaultValue = "1",value = "pagenum")int pagenum, Model model){
        PageHelper.startPage(pagenum, 5);
        List<User> allUser = userService.getAllUser();
        //得到分页结果对象
        PageInfo<User> pageInfo = new PageInfo<>(allUser);
        model.addAttribute("pageInfo", pageInfo);
        return "admin/user";
    }
    /**
     * 执行删除
     * @param id
     * @param attributes
     * @return
     */
    @GetMapping("/userAll/{id}/delete")
    public String delete(@PathVariable Integer id, RedirectAttributes attributes){
        userService.deleteUser(id);
        attributes.addFlashAttribute("msg", "删除成功");
        return "redirect:/admin/userAll";
    }
}
