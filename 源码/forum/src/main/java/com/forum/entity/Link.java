package com.forum.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 田勇
 * @date 2021/3/30 15:29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Link {
    private Integer id;
    private String name;
    private String address;
    private Date createTime;
}
