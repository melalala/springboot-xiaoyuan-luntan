package com.forum.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reply {
    private int id;
    private String names;
    private int mes_id;
    private String avatar;
    private String content;
    private Date createTime;
}
