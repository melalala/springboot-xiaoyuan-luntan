package com.forum.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//帖子排行封装
public class RankForum {
    long id;
    String title;
//    int lank;
}
