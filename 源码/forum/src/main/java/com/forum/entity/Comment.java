package com.forum.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    private Long id;
    private String username;
    private String email;
    private String content;
    //是否为用户评论
    private boolean userComment;
    //头像
    private String avatar;
    private Date createTime;
    private Long forumId;





    private Long parentCommentId;  //父评论id，是為1
//    private String parentNickname;

    //回复评论
    private List<Comment> replyComments = new ArrayList<>();
    //是否是用户评论
    private Comment parentComment;

    private Forum forum;

}
