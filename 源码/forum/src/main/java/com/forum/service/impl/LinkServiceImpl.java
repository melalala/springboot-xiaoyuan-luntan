package com.forum.service.impl;

import com.forum.dao.LinkDao;
import com.forum.entity.Link;
import com.forum.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author 田勇
 * @date 2021/3/30 15:32
 */
@Service
public class LinkServiceImpl implements LinkService
    {
        @Autowired
        private LinkDao linkDao;

        @Override
        public int saveLink(Link link) {
            link.setCreateTime(new Date());
            return linkDao.saveLink(link);
        }

        @Override
        public List<Link> getAllLink() {
            return linkDao.getAllLink();
        }

        @Override
        public int deleteLink(Integer id) {
            return linkDao.deleteLink(id);

        }

        @Override
        public int countLink() {
            return linkDao.countLink();
        }
    }
