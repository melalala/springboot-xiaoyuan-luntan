package com.forum.service.impl;

import com.forum.dao.ForumDao;
import com.forum.entity.*;
import com.forum.exception.NotFoundException;
import com.forum.service.ForumService;
import com.forum.service.TagService;
import com.forum.utils.MarkdownUtils;
//import com.forum.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ForumServiceImpl implements ForumService {

    @Autowired
    ForumDao forumDao;
    @Autowired
    TagService tagService;
//    @Autowired
//    RedisUtil redisUtil;

    private Boolean hasKey = false;

    @Override
    public void updateView(Long id) {
        forumDao.updateView(id);
    }

    @Override
    public Forum getForum(Long id) {

        return forumDao.getForum(id);
    }

    @Override
    public Forum getDetailedForum(Long id) {
        Forum forum = forumDao.getDetailedForum(id);
        if (forum == null) {
            throw new NotFoundException("该帖子不存在");
        }
        String content = forum.getContent();
        forum.setContent(MarkdownUtils.markdownToHtmlExtensions(content));  //将Markdown格式转换成html
        return forum;
    }

    @Override
    public List<Forum> getAllForum() {
        return forumDao.getAllForum();
    }

    @Override
    public List<Forum> getByTypeId(Long typeId) {
        return forumDao.getByTypeId(typeId);
    }

    @Override
    public List<Forum> getByTagId(Long tagId) {
        return forumDao.getByTagId(tagId);
    }

    @Override
    public List<Forum> getIndexForum() {
        List<Forum> forums = forumDao.getIndexForum();
//        int startPage=(pageNum-1)*5;
//        List<Forum> forums= forumDao.getForumTadIds();
//        System.out.println(forums.size());
//        List<Tag> tags;
//        for (Forum forum : forums) {
//            tags=new ArrayList<>();
//            for (TagIds id : forum.getTagsId()) {
//                tags.add(tagService.getTagById(id.getTag_id()));
//            }
//            forum.setTags(tags);
//
//        }

        return forums;
    }

    @Override
    public List<Forum> getAllRecommendForum() {
        return forumDao.getAllRecommendForum();
    }

    @Override
    public List<Forum> getSearchForum(String query) {
        return forumDao.getSearchForum(query);
    }

    @Override
    public Map<String, List<Forum>> archiveForum() {
        List<String> years;
        Map<String, List<Forum>> map = new LinkedHashMap<>();
        years = forumDao.findGroupYear();
        Set<String> set = new HashSet<>(years);  //set去掉重复的年份

//        转回list进行排序
        years = new ArrayList<>(set);
        Collections.sort(years);
        Collections.reverse(years);
//    遍历
        for (String year : years) {
            System.out.println(year);
            map.put(year, findForumAndTagsByYear(year));
        }
        return map;
    }

    /*
    根据查找文章的所有
     */
    public List<Forum> findForumAndTagsByYear(String year) {
        List<Forum> forumAndTagIds = forumDao.findForumAndTagsByYear(year);

//       将标签的id集合遍历查找出标签
        List<Tag> tags;
        for (Forum forumAndTagId : forumAndTagIds) {
            tags = new ArrayList<>();

//            System.out.println(tags.size());
            forumAndTagId.setTags(tags);
        }

        return forumAndTagIds;
    }

    @Override
    public int countForum() {
        return forumDao.getAllForum().size();
    }

    @Override
    public List<Forum> searchAllForum(Forum forum) {
        return forumDao.searchAllForum(forum);
    }


    /**
     * 排行榜信息
     *
     * @return
     */
    @Override
    public List<RankForum> findForumByLank() {

            List<RankForum> rankForums = new ArrayList<>();

            rankForums = forumDao.findForumByRank();

            return rankForums;

//
//        }else {
//            System.out.println("走REDIS---");
//            return (List<RankForum>) redisUtil.get("rank");
//
//        }

    }

    @Override
    public List<RankForum> getNewForum() {
        return forumDao.getNewForum();
    }

//    @Override
//    public Long getLike(Long id) {
//        return forumDao.getLike(id);
//    }

//    @Override
//    public void addLike(Long forumId, Long userId) {
////        forumDao.addLike(forumId);
////        forumDao.addForum_Like(forumId, userId);
//        System.out.println("执行插入Like_User");
//
//    }

//    @Override
//    public int getLikeByUser(Long userId, Long forumId) {
//        return forumDao.getLikeByUser(userId, forumId);
//    }


//    @Override
//    public void cancelLike(Long forumId, Long userId) {
//        forumDao.cancelLike(forumId);
//        forumDao.removeForum_Like(forumId, userId);
//    }

    @Override
    public int countView() {
        return forumDao.countView();
    }

    @Override
    public void addLike(Long id) {
        forumDao.addLike(id);
    }


    @Override    //新增帖子
    public int saveForum(Forum forum) {
        forum.setCreateTime(new Date());
        forum.setViews(0);
//        forum.setFlag("原创");
        //保存帖子
        forumDao.saveForum(forum);
        //保存帖子后才能获取自增的id
        Long id = forum.getId();
        //将标签的数据存到t_forums_tag表中
        List<Tag> tags = forum.getTags();
        ForumAndTag forumAndTag = null;
        for (Tag tag : tags) {
            //新增时无法获取自增的id,在mybatis里修改
            forumAndTag = new ForumAndTag(tag.getId(), id);
            forumDao.saveForumAndTag(forumAndTag);
        }
        return 1;
    }

    @Override   //编辑帖子
    public int updateForum(Forum forum) {
        forum.setUpdateTime(new Date());
        //将标签的数据存到t_forums_tag表中
        List<Tag> tags = forum.getTags();
        ForumAndTag forumAndTag = null;
        for (Tag tag : tags) {
            forumAndTag = new ForumAndTag(tag.getId(), forum.getId());
            forumDao.saveForumAndTag(forumAndTag);
        }
        return forumDao.updateForum(forum);
    }

    @Override
    public int deleteForum(Long id) {
        return forumDao.deleteForum(id);
    }


}
