package com.forum.service;

import com.forum.entity.Reply;

import java.util.List;

/**
 * @author 田勇
 * @date 2021/4/1 22:05
 */
public interface ReplyService {
    public List<Reply> findAll();

    void del(int id);
}
