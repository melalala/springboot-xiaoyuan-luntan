package com.forum.service.impl;

import com.forum.dao.UserDao;
import com.forum.entity.User;
import com.forum.entity.User;
import com.forum.service.UserService;
import com.forum.utils.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User checkUser(String username, String password) {
        User user = userDao.queryByUsernameAndPassword(username, MD5.code(password));
        return user;
    }

    @Override
    public void addUser(User user) {
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setType(0);
        user.setAvatar("https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3892165108,471848507&fm=26&gp=0.jpg");
        System.out.println(user);
        userDao.addUser(user);
    }

    @Override
    public List<User> getAllUser() {
        return userDao.getAllUser();
    }

    @Override
    public int deleteUser(int id) {
        return userDao.deleteUser(id);
    }

    @Override
    public int count() {
        return userDao.count();
    }

    @Override
    public void update(String password, String username) {
        userDao.update(password,username);
    }


}
