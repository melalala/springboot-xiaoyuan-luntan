package com.forum.service;

import com.forum.entity.Comment;



import java.util.List;

public interface CommentService {

    List<Comment> getCommentByForumId(Long forumId);

    int saveComment(Comment comment);

    List<Comment> getAllComment();

    void delById(Integer id);
}
