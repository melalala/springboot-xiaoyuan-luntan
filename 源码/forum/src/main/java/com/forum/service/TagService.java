package com.forum.service;

import com.forum.entity.Tag;

import java.util.List;

public interface TagService {

    int saveTag(Tag tag);//执行新增模块

    Tag getTag(Long id);

    Tag getTagByName(String name);
    Tag getTagById(int id);

    List<Tag> getAllTag();

    List<Tag> getForumTag();  //首页展示帖子类型

    List<Tag> getTagByString(String text);   //从字符串中获取tag集合

    int updateTag(Tag tag);

    int deleteTag(Long id);

    int countTag();
}
