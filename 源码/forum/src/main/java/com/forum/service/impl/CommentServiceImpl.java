package com.forum.service.impl;

import com.forum.dao.ForumDao;
import com.forum.dao.CommentDao;
import com.forum.entity.Comment;
import com.forum.service.CommentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private ForumDao forumDao;

    @Override
    public List<Comment> getCommentByForumId(Long forumId) {  //查询父评论
        //没有父节点的默认为-1
        List<Comment> comments = commentDao.findByForumIdAndParentCommentNull(forumId, Long.parseLong("-1"));

        return comments;
    }


    @Override
    //接收回复的表单
    public int saveComment(Comment comment) {
        //获得父id
        Long parentCommentId = comment.getParentComment().getId();
        System.out.println("是否是父评论"+parentCommentId);
        comment.setAvatar("https://wallpaperm.cmcm.com/aab1692cf70ed27d9ebcf0ca3e88d61d.jpg");

        //没有父级评论默认是-1
        if (parentCommentId != -1) {
            //有父级评论
//            comment.setParentComment(commentDao.findByParentCommentId(comment.getParentCommentId()));

            comment.setParentCommentId(parentCommentId);
        } else {
            //没有父级评论
            comment.setParentCommentId((long) 0);
//            comment.setParentComment(null);
        }
       comment.setCreateTime(new Date());
        return commentDao.saveComment(comment);
    }

    @Override
    public List<Comment> getAllComment() {
        return commentDao.getAllComment();
    }

    @Override
    public void delById(Integer id) {
        commentDao.delById(id);
    }
    /**
     * 循环每个父评论“1”
     */
//    private List<Comment> oneComment(List<Comment> comments){
//        List<Comment> commentsView = new ArrayList<>();
//        for (Comment comment : comments) {
//            Comment comment1 = new Comment();
//            BeanUtils.copyProperties(comment,comment1);
//            commentsView.add(comment1);
//
//        }
//        combineChildren(commentsView);
//        return commentsView;
//    }
//
//    /**
//     * 存储帖子为1的对象集合
//     * @param comments
//     */
//    private void combineChildren(List<Comment> comments) {
//        for (Comment comment : comments) {
//            List<Comment> replys1 = comment.getReplyComments();
//            for (Comment reply1 : replys1) {
//                recursively(reply1);
//            }
//            comment.setReplyComments(tempReplys);
//            tempReplys = new ArrayList<>();
//        }
//    }
//private List<Comment> tempReplys =new ArrayList<>();
//
//    /**
//     * 迭代的对象
//     * @param
//     */
//    private void recursively(Comment comment) {
//        tempReplys.add(comment);
//        if (comment.getReplyComments().size()>0){
//            List<Comment> replys = comment.getReplyComments();
//            for (Comment reply : replys) {
//                tempReplys.add(reply);
//                if (reply.getReplyComments().size()>0){
//                    recursively(reply);
//                }
//
//            }
//        }
//    }
}
