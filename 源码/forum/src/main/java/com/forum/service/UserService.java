package com.forum.service;


import com.forum.entity.User;

import java.util.List;

public interface UserService {

    public User checkUser(String username, String password);
    void addUser(User user);
    List<User> getAllUser();
    int deleteUser(int id);

    int count();


    void update(String password, String username);
}
