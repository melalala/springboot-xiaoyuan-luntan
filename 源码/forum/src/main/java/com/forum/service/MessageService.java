package com.forum.service;

import com.forum.entity.Message;
import com.forum.entity.Reply;

import java.util.List;

public interface MessageService {
    List<Message> findAllMessage();
    List<Message> getIndexMessage();
    void saveMessage(Message message);
    void saveReplyMessage(Reply reply);

    List<Message> findAll();

    void deleteById(int id);
}
