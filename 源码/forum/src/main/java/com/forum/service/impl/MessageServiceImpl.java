package com.forum.service.impl;

import com.forum.dao.MessageDao;
import com.forum.entity.Message;
import com.forum.entity.Reply;
import com.forum.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageDao messageDao;
    @Override
    public List<Message> findAllMessage() {
        return messageDao.findAllMessage();
    }

    @Override
    public List<Message> getIndexMessage() {
        List<Message> messages=messageDao.getIndexMessage();
        for (Message message : messages) {
            if (message.getContent().length()>10)
            message.setContent(message.getContent().substring(0,10)+"..");
        }
        return messages;
    }

    @Override
    public void saveMessage(Message message) {

        Date date=new Date();
        message.setCreateTime(date);
        messageDao.saveMessage(message);
    }

    @Override
    public void saveReplyMessage(Reply reply) {
        Date date=new Date();
        reply.setCreateTime(date);
        messageDao.saveReplyMessage(reply);
    }

    @Override
    public List<Message> findAll() {
        return messageDao.findAll();
    }

    @Override
    public void deleteById(int id) {
        messageDao.deleteById(id);
    }
}
