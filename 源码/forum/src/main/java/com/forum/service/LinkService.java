package com.forum.service;

import com.forum.entity.Link;

import java.util.List;

/**
 * @author 田勇
 * @date 2021/3/30 15:32
 */
public interface LinkService {
     int saveLink(Link link);

    List<Link> getAllLink();


    int deleteLink(Integer id);

    int countLink();
}
