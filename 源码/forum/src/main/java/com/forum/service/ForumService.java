package com.forum.service;

import com.forum.entity.Forum;
import com.forum.entity.RankForum;

import java.util.List;
import java.util.Map;

public interface ForumService {
    void updateView(Long id);

    Forum getForum(Long id);  //后台展示论坛

    Forum getDetailedForum(Long id);  //前端展示论坛

    List<Forum> getAllForum();

    List<Forum> getByTypeId(Long typeId);  //根据类型id获取论坛

    List<Forum> getByTagId(Long tagId);  //根据分类id获取论坛

    List<Forum> getIndexForum();  //主页论坛展示

    List<Forum> getAllRecommendForum();  //推荐论坛展示

    List<Forum> getSearchForum(String query);  //全局搜索论坛

    Map<String,List<Forum>> archiveForum();  //归档论坛

    int countForum();  //查询论坛条数
    //新增论坛
    int saveForum(Forum forum);

    int updateForum(Forum forum);

    int deleteForum(Long id);

    List<Forum> searchAllForum(Forum forum);  //后台根据标题、分类、推荐搜索论坛
//
    List<RankForum> findForumByLank();
    List<RankForum> getNewForum();
//    Long getLike(Long id);
//    void addLike(Long forumId,Long userId);
//    int getLikeByUser(Long userId,Long forumId);
//
////    void removeForum_Like(Long forumId,Long userId);
//    void cancelLike(Long forumId,Long userId);


    int countView();


    void addLike(Long id);
}
