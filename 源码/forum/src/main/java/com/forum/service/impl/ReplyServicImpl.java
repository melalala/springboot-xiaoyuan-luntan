package com.forum.service.impl;

import com.forum.dao.ReplyDao;
import com.forum.entity.Reply;
import com.forum.service.ReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 田勇
 * @date 2021/4/1 22:06
 */
@Service
public class ReplyServicImpl implements ReplyService {
    @Autowired
    ReplyDao replyDao;
    @Override
    public List<Reply> findAll() {
        return replyDao.findAll();
    }

    @Override
    public void del(int id) {
        replyDao.del(id);
    }
}
