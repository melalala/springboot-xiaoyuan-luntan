# 基于springboot实现的校园论坛系统

#### Description

运行环境
jdk7/8+maven+mysql5.6+IntelliJ IDEA+Navicat

技术点
springboot+mybatis+thymeleaf+layui

功能点
系统分为前后台两部分
前台实现了首页查看、分类帖子查看、发帖、公告查看、留言、最新推荐、热文推荐等功能；
后台实现了用户管理、首页访问数据统计、帖子管理、分类管理、通知管理、留言管理、回复评论管理功能

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
