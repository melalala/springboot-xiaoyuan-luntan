/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : localhost:3306
 Source Schema         : forum

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 10/12/2022 18:17:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `forum_id` bigint(20) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `parent_comment_id` bigint(20) NULL DEFAULT NULL,
  `usercomment` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 114 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (108, '1', '3602047188@qq.com', '*的皮肤好啊', 'https://wallpaperm.cmcm.com/aab1692cf70ed27d9ebcf0ca3e88d61d.jpg', '2021-04-15 18:01:59', 00000000000000000086, 0, 0);
INSERT INTO `comment` VALUES (109, '123', '3602047188@qq.com', '你滚吧', 'https://wallpaperm.cmcm.com/aab1692cf70ed27d9ebcf0ca3e88d61d.jpg', '2021-04-15 18:04:00', 00000000000000000086, 0, 0);
INSERT INTO `comment` VALUES (110, '123', '3602047188@qq.com', '滚吧', 'https://wallpaperm.cmcm.com/aab1692cf70ed27d9ebcf0ca3e88d61d.jpg', '2021-04-15 18:05:01', 00000000000000000086, 0, 0);
INSERT INTO `comment` VALUES (111, '123', '3602047188@qq.com', '*吧', 'https://wallpaperm.cmcm.com/aab1692cf70ed27d9ebcf0ca3e88d61d.jpg', '2021-04-15 18:06:05', 00000000000000000086, 0, 0);
INSERT INTO `comment` VALUES (112, '1', '3602047188@qq.com', '滚', NULL, '2021-04-16 15:50:15', 00000000000000000086, NULL, 0);
INSERT INTO `comment` VALUES (113, '用户3', '3602047188@qq.com', 'asda', 'https://wallpaperm.cmcm.com/aab1692cf70ed27d9ebcf0ca3e88d61d.jpg', '2021-04-19 12:16:25', 00000000000000000086, 0, 1);

-- ----------------------------
-- Table structure for forum
-- ----------------------------
DROP TABLE IF EXISTS `forum`;
CREATE TABLE `forum`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '帖子id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帖子标题',
  `like` int(2) UNSIGNED ZEROFILL NULL DEFAULT 00 COMMENT '帖子的点赞数',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '帖子是帖子的内容',
  `flag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '原创' COMMENT '帖子是否原创',
  `views` int(11) NULL DEFAULT NULL COMMENT '帖子是否阅读量',
  `share_statement` int(11) NULL DEFAULT 0 COMMENT '帖子是否分享',
  `commentabled` int(11) NULL DEFAULT 0 COMMENT '帖子是否评论',
  `published` int(11) NULL DEFAULT 0 COMMENT '帖子是否发布',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '帖子的创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '帖子的更新时间',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户的id',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '帖子的描述',
  `tag_ids` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帖子模块的分类id',
  `avatars` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帖子的图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 109 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of forum
-- ----------------------------
INSERT INTO `forum` VALUES (85, 'one', 01, '这是我的第一篇贴子，请多多关照', '原创', 16, 0, 0, 1, '2021-04-13 14:10:10', '2021-04-19 12:20:28', 35, '第一篇贴子', '19', 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=518155,1163243838&fm=26&gp=0.jpg');
INSERT INTO `forum` VALUES (86, '管理员发帖1', 00, '我是管理员发布的第一章贴子，我具有很多的权限哦', '原创', 33, 1, 1, 1, '2021-04-13 14:20:01', '2021-04-19 12:20:25', 36, '管理员发1', '19', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2759209514,2022059780&fm=26&gp=0.jpg');
INSERT INTO `forum` VALUES (87, 'two', 00, '我是用户1发布的第二个贴子，请多多关照[[1]][http://localhost:9999/publish]\r\n[http://localhost:9999/publish]: http://localhost:9999/publish \"发布帖子\"', '原创', 2, 0, 0, 1, '2021-04-13 14:59:01', '2021-04-19 12:20:23', 35, '第二个', '21', 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=32368453,742751200&fm=26&gp=0.jpg');
INSERT INTO `forum` VALUES (88, 'three', 01, '软件计算机技术', '转载', 5, 0, 0, 1, '2021-04-13 15:04:36', '2021-04-19 12:20:21', 35, '第三张帖子', '22', 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2364335342,608695125&fm=26&gp=0.jpg');
INSERT INTO `forum` VALUES (89, '520', 00, '# 我爱你哦  ', '原创', 2, 0, 0, 1, '2021-04-16 10:23:40', '2021-04-16 10:23:40', 35, '爱情日', '20', 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=343682999,1827287392&fm=26&gp=0.jpg');
INSERT INTO `forum` VALUES (90, '1314', 00, '# 对方答复\r\n## 地方个梵蒂冈 \r\n\r\n#### 4dfssgds\r\n', '原创', 4, 0, 0, 1, '2021-04-16 10:25:24', '2021-04-16 10:25:24', 35, '一生一世', '20', 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=4160833478,188817367&fm=26&gp=0.jpg');
INSERT INTO `forum` VALUES (91, '1', 00, '[imCoding 爱编程](http://www.lirenmi.cn)', '原创', 3, 0, 0, 1, '2021-04-16 11:00:23', '2021-04-16 11:00:23', 35, '1', '19', 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1798891010,1657314566&fm=26&gp=0.jpg');
INSERT INTO `forum` VALUES (92, 'markdown编辑器的使用介绍', 00, '\r\n# 欢迎使用Markdown编辑器\r\n\r\n你好！ 这是你第一次使用 **Markdown编辑器** 所展示的欢迎页。如果你想学习如何使用Markdown编辑器, 可以仔细阅读这篇文章，了解一下Markdown的基本语法知识。\r\n\r\n## 新的改变\r\n\r\n我们对Markdown编辑器进行了一些功能拓展与语法支持，除了标准的Markdown编辑器功能，我们增加了如下几点新功能，帮助你用它写博客：\r\n 1. **全新的界面设计** ，将会带来全新的写作体验；\r\n 2. 在创作中心设置你喜爱的代码高亮样式，Markdown **将代码片显示选择的高亮样式** 进行展示；\r\n 3. 增加了 **图片拖拽** 功能，你可以将本地的图片直接拖拽到编辑区域直接展示；\r\n 4. 全新的 **KaTeX数学公式** 语法；\r\n 5. 增加了支持**甘特图的mermaid语法[^1]** 功能；\r\n 6. 增加了 **多屏幕编辑** Markdown文章功能；\r\n 7. 增加了 **焦点写作模式、预览模式、简洁写作模式、左右区域同步滚轮设置** 等功能，功能按钮位于编辑区域与预览区域中间；\r\n 8. 增加了 **检查列表** 功能。\r\n [^1]: [mermaid语法说明](https://mermaidjs.github.io/)\r\n\r\n## 功能快捷键\r\n\r\n撤销：<kbd>Ctrl/Command</kbd> + <kbd>Z</kbd>\r\n重做：<kbd>Ctrl/Command</kbd> + <kbd>Y</kbd>\r\n加粗：<kbd>Ctrl/Command</kbd> + <kbd>B</kbd>\r\n斜体：<kbd>Ctrl/Command</kbd> + <kbd>I</kbd>\r\n标题：<kbd>Ctrl/Command</kbd> + <kbd>Shift</kbd> + <kbd>H</kbd>\r\n无序列表：<kbd>Ctrl/Command</kbd> + <kbd>Shift</kbd> + <kbd>U</kbd>\r\n有序列表：<kbd>Ctrl/Command</kbd> + <kbd>Shift</kbd> + <kbd>O</kbd>\r\n检查列表：<kbd>Ctrl/Command</kbd> + <kbd>Shift</kbd> + <kbd>C</kbd>\r\n插入代码：<kbd>Ctrl/Command</kbd> + <kbd>Shift</kbd> + <kbd>K</kbd>\r\n插入链接：<kbd>Ctrl/Command</kbd> + <kbd>Shift</kbd> + <kbd>L</kbd>\r\n插入图片：<kbd>Ctrl/Command</kbd> + <kbd>Shift</kbd> + <kbd>G</kbd>\r\n查找：<kbd>Ctrl/Command</kbd> + <kbd>F</kbd>\r\n替换：<kbd>Ctrl/Command</kbd> + <kbd>G</kbd>\r\n\r\n## 合理的创建标题，有助于目录的生成\r\n\r\n直接输入1次<kbd>#</kbd>，并按下<kbd>space</kbd>后，将生成1级标题。\r\n输入2次<kbd>#</kbd>，并按下<kbd>space</kbd>后，将生成2级标题。\r\n以此类推，我们支持6级标题。有助于使用`TOC`语法后生成一个完美的目录。\r\n\r\n## 如何改变文本的样式\r\n\r\n*强调文本* _强调文本_\r\n\r\n**加粗文本** __加粗文本__\r\n\r\n==标记文本==\r\n\r\n~~删除文本~~\r\n\r\n> 引用文本\r\n\r\nH~2~O is是液体。\r\n\r\n2^10^ 运算结果是 1024.\r\n\r\n## 插入链接与图片\r\n\r\n链接: [link](https://www.csdn.net/).\r\n\r\n图片: ![Alt](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9hdmF0YXIuY3Nkbi5uZXQvNy83L0IvMV9yYWxmX2h4MTYzY29tLmpwZw)\r\n\r\n带尺寸的图片: ![Alt](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9hdmF0YXIuY3Nkbi5uZXQvNy83L0IvMV9yYWxmX2h4MTYzY29tLmpwZw =30x30)\r\n\r\n居中的图片: ![Alt](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9hdmF0YXIuY3Nkbi5uZXQvNy83L0IvMV9yYWxmX2h4MTYzY29tLmpwZw#pic_center)\r\n\r\n居中并且带尺寸的图片: ![Alt](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9hdmF0YXIuY3Nkbi5uZXQvNy83L0IvMV9yYWxmX2h4MTYzY29tLmpwZw#pic_center =30x30)\r\n\r\n当然，我们为了让用户更加便捷，我们增加了图片拖拽功能。\r\n\r\n## 如何插入一段漂亮的代码片\r\n\r\n去[博客设置](https://mp.csdn.net/console/configBlog)页面，选择一款你喜欢的代码片高亮样式，下面展示同样高亮的 `代码片`.\r\n```javascript\r\n// An highlighted block\r\nvar foo = \'bar\';\r\n```\r\n\r\n## 生成一个适合你的列表\r\n\r\n- 项目\r\n  - 项目\r\n    - 项目\r\n\r\n1. 项目1\r\n2. 项目2\r\n3. 项目3\r\n\r\n- [ ] 计划任务\r\n- [x] 完成任务\r\n\r\n## 创建一个表格\r\n一个简单的表格是这么创建的：\r\n项目     | Value\r\n-------- | -----\r\n电脑  | $1600\r\n手机  | $12\r\n导管  | $1\r\n\r\n### 设定内容居中、居左、居右\r\n使用`:---------:`居中\r\n使用`:----------`居左\r\n使用`----------:`居右\r\n| 第一列       | 第二列         | 第三列        |\r\n|:-----------:| -------------:|:-------------|\r\n| 第一列文本居中 | 第二列文本居右  | 第三列文本居左 |\r\n\r\n### SmartyPants\r\nSmartyPants将ASCII标点字符转换为“智能”印刷标点HTML实体。例如：\r\n|    TYPE   |ASCII                          |HTML\r\n|----------------|-------------------------------|-----------------------------|\r\n|Single backticks|`\'Isn\'t this fun?\'`            |\'Isn\'t this fun?\'            |\r\n|Quotes          |`\"Isn\'t this fun?\"`            |\"Isn\'t this fun?\"            |\r\n|Dashes          |`-- is en-dash, --- is em-dash`|-- is en-dash, --- is em-dash|\r\n\r\n## 创建一个自定义列表\r\nMarkdown\r\n:  Text-to-HTML conversion tool\r\n\r\nAuthors\r\n:  John\r\n:  Luke\r\n\r\n## 如何创建一个注脚\r\n\r\n一个具有注脚的文本。[^2]\r\n\r\n[^2]: 注脚的解释\r\n\r\n##  注释也是必不可少的\r\n\r\nMarkdown将文本转换为 HTML。\r\n\r\n*[HTML]:   超文本标记语言\r\n\r\n## KaTeX数学公式\r\n\r\n您可以使用渲染LaTeX数学表达式 [KaTeX](https://khan.github.io/KaTeX/):\r\n\r\nGamma公式展示 $\\Gamma(n) = (n-1)!\\quad\\forall\r\nn\\in\\mathbb N$ 是通过欧拉积分\r\n\r\n$$\r\n\\Gamma(z) = \\int_0^\\infty t^{z-1}e^{-t}dt\\,.\r\n$$\r\n\r\n> 你可以找到更多关于的信息 **LaTeX** 数学表达式[here][1].\r\n\r\n## 新的甘特图功能，丰富你的文章\r\n\r\n```mermaid\r\ngantt\r\n        dateFormat  YYYY-MM-DD\r\n        title Adding GANTT diagram functionality to mermaid\r\n        section 现有任务\r\n        已完成               :done,    des1, 2014-01-06,2014-01-08\r\n        进行中               :active,  des2, 2014-01-09, 3d\r\n        计划一               :         des3, after des2, 5d\r\n        计划二               :         des4, after des3, 5d\r\n```\r\n- 关于 **甘特图** 语法，参考 [这儿][2],\r\n\r\n## UML 图表\r\n\r\n可以使用UML图表进行渲染。 [Mermaid](https://mermaidjs.github.io/). 例如下面产生的一个序列图：\r\n\r\n```mermaid\r\nsequenceDiagram\r\n张三 ->> 李四: 你好！李四, 最近怎么样?\r\n李四-->>王五: 你最近怎么样，王五？\r\n李四--x 张三: 我很好，谢谢!\r\n李四-x 王五: 我很好，谢谢!\r\nNote right of 王五: 李四想了很长时间, 文字太长了<br/>不适合放在一行.\r\n\r\n李四-->>张三: 打量着王五...\r\n张三->>王五: 很好... 王五, 你怎么样?\r\n```\r\n\r\n这将产生一个流程图。:\r\n\r\n```mermaid\r\ngraph LR\r\nA[长方形] -- 链接 --> B((圆))\r\nA --> C(圆角长方形)\r\nB --> D{菱形}\r\nC --> D\r\n```\r\n\r\n- 关于 **Mermaid** 语法，参考 [这儿][3],\r\n\r\n## FLowchart流程图\r\n\r\n我们依旧会支持flowchart的流程图：\r\n```mermaid\r\nflowchat\r\nst=>start: 开始\r\ne=>end: 结束\r\nop=>operation: 我的操作\r\ncond=>condition: 确认？\r\n\r\nst->op->cond\r\ncond(yes)->e\r\ncond(no)->op\r\n```\r\n\r\n- 关于 **Flowchart流程图** 语法，参考 [这儿][4].\r\n\r\n## 导出与导入\r\n\r\n###  导出\r\n如果你想尝试使用此编辑器, 你可以在此篇文章任意编辑。当你完成了一篇文章的写作, 在上方工具栏找到 **文章导出** ，生成一个.md文件或者.html文件进行本地保存。\r\n\r\n### 导入\r\n如果你想加载一篇你写过的.md文件，在上方工具栏可以选择导入功能进行对应扩展名的文件导入，\r\n继续你的创作。\r\n\r\n [1]: http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference\r\n [2]: https://mermaidjs.github.io/\r\n [3]: https://mermaidjs.github.io/\r\n [4]: http://adrai.github.io/flowchart.js/', '原创', 29, 0, 0, 1, '2021-04-16 11:12:47', '2021-04-16 11:12:47', 35, '免费学习一个编辑器', '24', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=289219702,213003764&fm=26&gp=0.jpg');
INSERT INTO `forum` VALUES (93, 'html', 01, '	public static void main(String[] args) {\r\n        String table = \"| hello | hi   | 哈哈哈   |\\n\" +\r\n                \"| ----- | ---- | ----- |\\n\" +\r\n                \"| 斯维尔多  | 士大夫  | f啊    |\\n\" +\r\n                \"| 阿什顿发  | 非固定杆 | 撒阿什顿发 |\\n\" +\r\n                \"\\n\";\r\n        String a = \"[imCoding 爱编程](http://www.lirenmi.cn)\";\r\n        System.out.println(markdownToHtmlExtensions(a));\r\n    }', '原创', 13, 0, 0, 1, '2021-04-16 12:51:23', '2021-04-16 12:51:23', 35, 'html', '24', 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3746221056,3542620044&fm=26&gp=0.jpg');
INSERT INTO `forum` VALUES (94, '维普', 00, '维普网，建立于2000年。经过多年的商业运营，维普网已经成为全球著名的中文专业信息服务网站。\r\n网站陆续建立了与谷歌学术搜索频道、百度文库、百度百科的战略合作关系。网站遥遥领先数字出版行业发展水平，数次名列中国出版业网站百强，并在中国图书馆业、情报业网站排名中名列前茅。 [1]', '原创', 7, 0, 0, 1, '2021-04-16 19:18:13', '2021-04-19 12:20:17', 43, '监测是否有重复的论文网站', '24', 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3089218110,1206531033&fm=15&gp=0.jpg');
INSERT INTO `forum` VALUES (104, '你最喜欢的书？', 00, 'sdgsdgsd', '', 1, 0, 0, 0, '2022-12-10 17:35:41', NULL, 46, '测试', '22', '');
INSERT INTO `forum` VALUES (105, 'test', 00, 'test', '', 0, 0, 0, 0, '2022-12-10 17:39:19', NULL, 46, 'test', '21', 'https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=d&word=%E6%98%8E%E6%98%9F%E5%9B%BE%E7%89%87&step_word=&hs=0&pn=5&spn=0&di=7169026086108397569&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=0&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=undefined&cs=306213251%2C1092921947&os=29179580%2C1085282361&simid=4220626036%2C518472862&adpicid=0&lpn=0&ln=1762&fr=&fmq=1670665132525_R&fm=&ic=undefined&s=undefined&hd=undefined&latest=undefined&copyright=undefined&se=&sme=&tab=0&width=undefined&height=undefined&face=undefined&ist=&jit=&cg=&bdtype=0&oriquery=&objurl=https%3A%2F%2Fhbimg.b0.upaiyun.com%2F344d01da970161299ed072270667c04e1607dead48de6-YWVM0o_fw658&fromurl=ippr_z2C%24qAzdH3FAzdH3Fi7wkwg_z%26e3Bv54AzdH3FrtgfAzdH3F0ncb88cncAzdH3F&gsm=1e&rpstart=0&rpnum=0&islist=&querylist=&nojc=undefined&dyTabStr=MCwzLDYsMSw0LDUsOCw3LDIsOQ%3D%3D');
INSERT INTO `forum` VALUES (106, 'test22', 00, '234346467457', '', 0, 0, 0, 0, '2022-12-10 17:40:11', NULL, 46, 'test22', '20', 'user.jpg');
INSERT INTO `forum` VALUES (107, 'test444', 00, '23534yteddgdfhfrrhjdg', '', 1, 0, 0, 0, '2022-12-10 17:50:55', NULL, 46, 'asdfsgsd', '19', 'http://localhost/storage/code2life/8cm6l3bw9c5ys12rac8y.jpg');
INSERT INTO `forum` VALUES (108, '1111111', 00, 'sdgdshdfhndfn\r\ndgdfhd', '', 9, 0, 0, 0, '2022-12-10 17:53:17', NULL, 46, '111111111', '23', 'http://localhost/storage/code2life/pnv8t4izme9lcvl91l8p.jpg');

-- ----------------------------
-- Table structure for forum_tags
-- ----------------------------
DROP TABLE IF EXISTS `forum_tags`;
CREATE TABLE `forum_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) NULL DEFAULT NULL,
  `forum_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of forum_tags
-- ----------------------------
INSERT INTO `forum_tags` VALUES (151, 20, '69');
INSERT INTO `forum_tags` VALUES (152, 20, '70');
INSERT INTO `forum_tags` VALUES (153, 20, '71');
INSERT INTO `forum_tags` VALUES (154, 20, '72');
INSERT INTO `forum_tags` VALUES (155, 19, '73');
INSERT INTO `forum_tags` VALUES (156, 21, '74');
INSERT INTO `forum_tags` VALUES (157, 22, '76');
INSERT INTO `forum_tags` VALUES (158, 21, '77');
INSERT INTO `forum_tags` VALUES (159, 23, '78');
INSERT INTO `forum_tags` VALUES (160, 19, '79');
INSERT INTO `forum_tags` VALUES (161, 23, '80');
INSERT INTO `forum_tags` VALUES (162, 20, '81');
INSERT INTO `forum_tags` VALUES (163, 20, '82');
INSERT INTO `forum_tags` VALUES (164, 23, '83');
INSERT INTO `forum_tags` VALUES (165, 19, '84');
INSERT INTO `forum_tags` VALUES (166, 19, '85');
INSERT INTO `forum_tags` VALUES (167, 19, '86');
INSERT INTO `forum_tags` VALUES (168, 21, '87');
INSERT INTO `forum_tags` VALUES (169, 22, '88');
INSERT INTO `forum_tags` VALUES (170, 20, '89');
INSERT INTO `forum_tags` VALUES (171, 20, '90');
INSERT INTO `forum_tags` VALUES (172, 19, '91');
INSERT INTO `forum_tags` VALUES (173, 24, '92');
INSERT INTO `forum_tags` VALUES (174, 24, '93');
INSERT INTO `forum_tags` VALUES (175, 24, '94');
INSERT INTO `forum_tags` VALUES (176, 19, '95');
INSERT INTO `forum_tags` VALUES (177, 19, '96');
INSERT INTO `forum_tags` VALUES (178, 20, '97');
INSERT INTO `forum_tags` VALUES (179, 19, '98');
INSERT INTO `forum_tags` VALUES (180, 19, '99');
INSERT INTO `forum_tags` VALUES (181, 19, '101');
INSERT INTO `forum_tags` VALUES (182, 19, '102');
INSERT INTO `forum_tags` VALUES (183, 19, '103');
INSERT INTO `forum_tags` VALUES (184, 19, '104');
INSERT INTO `forum_tags` VALUES (185, 19, '105');
INSERT INTO `forum_tags` VALUES (186, 19, '106');
INSERT INTO `forum_tags` VALUES (187, 20, '107');
INSERT INTO `forum_tags` VALUES (188, 19, '108');
INSERT INTO `forum_tags` VALUES (189, 19, '109');
INSERT INTO `forum_tags` VALUES (190, 19, '110');
INSERT INTO `forum_tags` VALUES (191, 19, '111');
INSERT INTO `forum_tags` VALUES (192, 19, '112');
INSERT INTO `forum_tags` VALUES (193, 19, '113');
INSERT INTO `forum_tags` VALUES (194, 20, '114');
INSERT INTO `forum_tags` VALUES (195, 22, '104');
INSERT INTO `forum_tags` VALUES (196, 21, '105');
INSERT INTO `forum_tags` VALUES (197, 20, '106');
INSERT INTO `forum_tags` VALUES (198, 19, '107');
INSERT INTO `forum_tags` VALUES (199, 23, '108');

-- ----------------------------
-- Table structure for link
-- ----------------------------
DROP TABLE IF EXISTS `link`;
CREATE TABLE `link`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '友链id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '友链名称',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '友链地址',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT 'shijian',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of link
-- ----------------------------
INSERT INTO `link` VALUES (2, 'CSDN', 'https://www.csdn.net/', '2021-03-30 19:13:51');
INSERT INTO `link` VALUES (3, 'QQ', 'https://im.qq.com/', '2021-03-30 19:13:53');
INSERT INTO `link` VALUES (5, '知乎', 'https://www.zhihu.com/signin?next=%2F', '2021-03-30 19:13:56');
INSERT INTO `link` VALUES (6, '新浪', 'https://www.sina.com.cn/', '2021-03-30 19:13:59');
INSERT INTO `link` VALUES (7, '微博', 'https://weibo.com/', '2021-03-30 19:14:01');
INSERT INTO `link` VALUES (8, '微信', 'www.wechat.com', '2021-03-31 09:18:57');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '留言的id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '留言的内容',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '留言者的昵称',
  `create_time` datetime(0) NOT NULL COMMENT '留言的时间',
  `msg_avatar` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'http://img.duoziwang.com/2018/17/05272146700399.jpg' COMMENT '留言者的头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES (29, '亲，我来了\r\n', '用户3', '2021-04-19 17:09:49', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=302460632,2916540262&fm=26&gp=0.jpg');
INSERT INTO `message` VALUES (30, '我的用户之一', '用户3', '2021-04-19 17:11:50', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=302460632,2916540262&fm=26&gp=0.jpg');
INSERT INTO `message` VALUES (31, 'qq', '用户3', '2021-04-20 09:42:46', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=302460632,2916540262&fm=26&gp=0.jpg');
INSERT INTO `message` VALUES (32, 'gjhbj', 'demo', '2022-12-10 17:56:53', 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3892165108,471848507&fm=26&gp=0.jpg');
INSERT INTO `message` VALUES (33, '这不行啊', 'demo', '2022-12-10 18:17:13', 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3892165108,471848507&fm=26&gp=0.jpg');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '通知的id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '通知的内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES (2, '界面有待优化！');
INSERT INTO `notice` VALUES (3, '后面会跟进项目和论文的书写');
INSERT INTO `notice` VALUES (4, '清明节来了哦！');
INSERT INTO `notice` VALUES (6, '520');

-- ----------------------------
-- Table structure for reply
-- ----------------------------
DROP TABLE IF EXISTS `reply`;
CREATE TABLE `reply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '\r\n回复的id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '回复的内容',
  `names` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '回复者的昵称',
  `create_time` datetime(0) NOT NULL COMMENT '回复的时间',
  `mes_id` int(11) NOT NULL COMMENT '评论的id',
  `avatar` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2902069684,3620245990&fm=26&gp=0.jpg',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reply
-- ----------------------------
INSERT INTO `reply` VALUES (35, 'dsfdasfads ', '用户3', '2021-04-19 00:00:00', 25, 'https://wallpaperm.cmcm.com/aab1692cf70ed27d9ebcf0ca3e88d61d.jpg');
INSERT INTO `reply` VALUES (36, ' 地方桑', '用户3', '2021-04-19 17:12:06', 30, 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=302460632,2916540262&fm=26&gp=0.jpg');
INSERT INTO `reply` VALUES (37, '1', '用户3', '2021-04-20 09:42:53', 31, 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=302460632,2916540262&fm=26&gp=0.jpg');

-- ----------------------------
-- Table structure for tag
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '分类的id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类的名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tag
-- ----------------------------
INSERT INTO `tag` VALUES (19, 'IT类');
INSERT INTO `tag` VALUES (20, '文学类');
INSERT INTO `tag` VALUES (21, '体育类');
INSERT INTO `tag` VALUES (22, '国考类');
INSERT INTO `tag` VALUES (23, '美术类');
INSERT INTO `tag` VALUES (24, '理工类');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户的id',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户的用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户的登录密码',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户的邮箱',
  `avatar` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户的头像',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '用户的注册时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '用户的更新时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '用戶的類型 1 管理員 2 用戶',
  `school` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学校',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (35, '用户1', '202cb962ac59075b964b07152d234b70', '3602047188@qq.com', 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1713777766,1496907822&fm=15&gp=0.jpg', '2021-04-13 13:53:10', '2021-04-13 13:53:10', 0, '贵州工程应用技术学院');
INSERT INTO `user` VALUES (36, '管理员', 'e10adc3949ba59abbe56e057f20f883e', '3602047188@qq.com', 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=36190078,3023005882&fm=26&gp=0.jpg', '2021-04-13 14:18:04', '2021-04-13 14:18:08', 1, '我是管理员哦');
INSERT INTO `user` VALUES (42, '用户2', 'd41d8cd98f00b204e9800998ecf8427e', '3602047188@qq.com', 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3773787307,2907114139&fm=26&gp=0.jpg', '2021-04-16 14:44:05', '2021-04-16 14:44:05', 0, '贵州工程应用技术学院');
INSERT INTO `user` VALUES (43, '用户3', 'd41d8cd98f00b204e9800998ecf8427e', '3602047188@qq.com', 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=302460632,2916540262&fm=26&gp=0.jpg', '2021-04-16 14:45:47', '2021-04-16 14:45:47', 0, '贵州工程应用技术学院');
INSERT INTO `user` VALUES (46, 'demo', '202cb962ac59075b964b07152d234b70', 'demo@qq.com', 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3892165108,471848507&fm=26&gp=0.jpg', '2022-12-10 17:34:57', '2022-12-10 17:34:57', 0, '武汉大学');

SET FOREIGN_KEY_CHECKS = 1;
