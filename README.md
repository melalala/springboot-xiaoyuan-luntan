# 基于springboot实现的校园论坛系统

#### 介绍

运行环境
jdk7/8+maven+mysql5.6+IntelliJ IDEA+Navicat

技术点
springboot+mybatis+thymeleaf+layui

功能点
系统分为前后台两部分
前台实现了首页查看、分类帖子查看、发帖、公告查看、留言、最新推荐、热文推荐等功能；
后台实现了用户管理、首页访问数据统计、帖子管理、分类管理、通知管理、留言管理、回复评论管理功能

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
